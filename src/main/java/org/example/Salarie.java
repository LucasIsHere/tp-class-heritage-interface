package org.example;

public class Salarie {

    public String matricule;
    public String categorie;
    public String service;
    public String name;
    public float salaire;
    public static int compteurInstance = 0;

    public Salarie() {
    }

    public Salarie(String name, float salaire) {
        this.name = name;
        this.salaire = salaire;
        ++compteurInstance;
    }

    public Salarie(String name, String matricule, String categorie, String service, float salaire) {
        this.name = name;
        this.matricule = matricule;
        this.categorie = categorie;
        this.service = service;
        this.salaire = salaire;
    }

    public static void setCompteurInstance(int compteurInstance) {
        Salarie.compteurInstance = compteurInstance;
    }

    public static int getCompteurInstance() {
        return compteurInstance;
    }

    public void afficherSalaire() {
        System.out.println("Le salaire fixe de " + name + " est de " + salaire + "€.");
    }

    public static int somme(float... floats) {
        int res = 0;
        for (float i : floats) {
            res += i;
        }
        return res;
    }

    public float calculSalaire() {
        return salaire;
    }

    @Override
    public String toString() {
        return "Salarie{" +
                "matricule='" + matricule + '\'' +
                ", categorie='" + categorie + '\'' +
                ", service='" + service + '\'' +
                ", name='" + name + '\'' +
                ", salaire=" + salaire +
                "}\n";
    }
}
