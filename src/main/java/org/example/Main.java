package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Salarie sal1 = new Salarie("Xavier", 1500);
        Salarie sal2 = new Salarie("Rémi", 2315);
        Commercial com1 = new Commercial("Claude", 1800, 8);
        Commercial com2 = new Commercial("Patrick", 2700, 11);

//        Salarie[] tabSalaries = {sal1, sal2, com1, com2};

        List<Salarie> listSalaries = new ArrayList<Salarie>();
        listSalaries.add(sal1);
        listSalaries.add(sal2);
        listSalaries.add(com1);
        listSalaries.add(com2);

        boolean saisieValid = false;
        boolean saisieValid2 = false;

        Scanner action = new Scanner(System.in);
        Scanner inputName = new Scanner(System.in);
        Scanner inputMatricule = new Scanner(System.in);
        Scanner inputCategorie = new Scanner(System.in);
        Scanner inputService = new Scanner(System.in);
        Scanner inputSalaire = new Scanner(System.in);

        do {
            System.out.println("\n===== Gestion des employés =====\n");
            System.out.println("1 - Ajouter un employé");
            System.out.println("2 - Afficher le salaire des employés");
            System.out.println("3 - Rechercher un employé");
            System.out.println("0 - Quitter le programme\n");

            System.out.print("(Menu principal) Entrez votre choix : ");
            int choix = action.nextInt();
            switch (choix) {
                case 1:
                    saisieValid = false;
                    System.out.println("\n===== Ajouter un employé =====\n");
                    System.out.println("1 - Salarié");
                    System.out.println("2 - Commercial");
                    System.out.println("0 - Retour\n");

                    do {
                        System.out.print("(Menu ajout salarié) Entrez votre choix : ");
                        choix = action.nextInt();

                        switch (choix) {
                            case 1:
                                saisieValid2 = true;
                                System.out.println("\n----- Ajout d'un salarié -----");
                                System.out.print("\nMerci de saisir le nom : ");
                                String name = inputName.next();
                                System.out.print("Merci de saisir le matricule : ");
                                String matricule = inputMatricule.next();
                                System.out.print("Merci de saisir la catégorie : ");
                                String categorie = inputCategorie.next();
                                System.out.print("Merci de saisir le service : ");
                                String service = inputService.next();
                                System.out.print("Merci de saisir le salaire : ");
                                float salaire = inputSalaire.nextFloat();

                                Salarie newSalarie = new Salarie(name, matricule, categorie, service, salaire);
                                listSalaries.add(newSalarie);
                                System.out.println("\n" + newSalarie);
                                break;
                            case 2:
                                saisieValid2 = true;
                                System.out.println("\n----- Ajout d'un commercial -----");
                                System.out.print("\nMerci de saisir le nom : ");
                                name = inputName.next();
                                System.out.print("Merci de saisir le matricule : ");
                                matricule = inputMatricule.next();
                                System.out.print("Merci de saisir la catégorie : ");
                                categorie = inputCategorie.next();
                                System.out.print("Merci de saisir le service : ");
                                service = inputService.next();
                                System.out.print("Merci de saisir le salaire : ");
                                salaire = inputSalaire.nextFloat();
                                System.out.print("Merci de saisir le chiffre d'affaire : ");
                                float chiffreAffaire = inputSalaire.nextFloat();
                                System.out.print("Merci de saisir la commission : ");
                                int commission = inputSalaire.nextInt();

                                Commercial newCommercial = new Commercial(name, matricule, categorie, service, salaire, chiffreAffaire, commission);
                                listSalaries.add(newCommercial);
                                System.out.println("\n" + newCommercial);
                                break;
                            case 0:
                                saisieValid2 = true;
                                System.out.println("Retour au menu principal...");
                                saisieValid = false;
                                break;
                            default:
                                saisieValid2 = false;
                                System.out.println("Saisie invalide.");
                                break;
                        }
                    } while (saisieValid2 == false);
                    break;
                case 2:
                    saisieValid = false;
                    System.out.println("\n===== Salaire des employés =====\n");
//                    for (int i = 0; i < tabSalaries.length; i++) {
//                        tabSalaries[i].afficherSalaire();
//                    }
                    for (Salarie salaries : listSalaries) {
                        salaries.afficherSalaire();
                    }

                    break;
                case 3:
                    saisieValid = false;
                    boolean noMatchFound = false;
                    System.out.println("\n===== Recherche employé par nom =====\n");
                    System.out.print("Merci de saisir le nom : ");
                    String name = inputName.next();
                    System.out.println("");

//                    for (int i = 0; i < tabSalaries.length; i++) {
//                        if (tabSalaries[i].name.equals(name)) {
//                            noMatchFound = false;
//                            tabSalaries[i].afficherSalaire();
//                            break;
//                        } else {
//                            noMatchFound = true;
//                        }
//                    }

                    for (Salarie salaries : listSalaries) {
                        if (salaries.name.equals(name)) {
                            noMatchFound = false;
                            salaries.afficherSalaire();
                            break;
                        } else {
                            noMatchFound = true;
                        }
                    }

                    if (noMatchFound == true) {
                        System.out.println("Aucun salarié avec ce nom n'a été trouvé.");
                    }
                    break;
                case 0:
                    saisieValid = true;
                    System.out.println("Fermeture du programme...");
                    break;
                default:
                    saisieValid = false;
                    System.out.println("Saisie invalide.");
                    break;
            }
        } while (saisieValid == false);
    }
}