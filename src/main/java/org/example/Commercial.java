package org.example;

public class Commercial extends Salarie {

    public float chiffreAffaire;
    public int commission;
    private final float POURCENTAGE = 100;
    public float distanceParcourue = 0;

    public Commercial(String name, float salaire) {
        super(name, salaire);
    }

    public Commercial(String name, float salaire, int commission) {
        super(name, salaire);
        this.commission = commission;
    }

    public Commercial(String name, String matricule, String categorie, String service, float salaire, float chiffreAffaire, int commission) {
        super(name, matricule, categorie, service, salaire);
        this.chiffreAffaire = chiffreAffaire;
        this.commission = commission;
    }

    public float getChiffreAffaire() {
        return chiffreAffaire;
    }

    public void setChiffreAffaire(float chiffreAffaire) {
        this.chiffreAffaire = chiffreAffaire;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    @Override
    public float calculSalaire() {
        salaire = (salaire + (salaire * (commission / POURCENTAGE)));
        return salaire;
    }

    public void seDeplacer() {
        distanceParcourue += 1000;
    }

    @Override
    public void afficherSalaire() {
        super.afficherSalaire();
        System.out.println("Le salaire avec commission de " + name + " est de " + calculSalaire() + "€.");
    }

    @Override
    public String toString() {
        return "Commercial{" +
                "chiffreAffaire=" + chiffreAffaire +
                ", commission=" + commission +
                ", distanceParcourue=" + distanceParcourue +
                ", matricule='" + matricule + '\'' +
                ", categorie='" + categorie + '\'' +
                ", service='" + service + '\'' +
                ", name='" + name + '\'' +
                ", salaire=" + salaire +
                "}\n";
    }
}
